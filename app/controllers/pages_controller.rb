class PagesController < ApplicationController
  def about_us
  end

  def courses
  end

  def locations
  end

  def contact_us
  end

  def home
  end
end
